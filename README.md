# IOASYS Books

Esse projeto foi desenvolvido como um desafio e faz a listagem de livros feitas através de uma requisição na API

## Scripts Disponíveis

No diretório desse projeto você pode executar os seguintes comandos

### `yarn start`

Executa esse app em modo de desenvolvimento.\ <br>
Abra [http://localhost:3000](http://localhost:3000) para ver o app funcionando

Essa é uma previsualização da aplicação:

<br/>

<img src="./public/img/ioasysDemo.gif" />

<br/>

# Instalação

Após baixar ou clonar o repositório, você deve baixar as dependências para que a aplicação possa funcionar direito:

<br/>

usando NPM - Node Package Manager ou o Yarn: 

```sh
npm install
```
using Yarn
```sh
yarn add
```

<br/>


# License
© Feito com muito &#10084; por [Ramon Pereira](https://www.linkedin.com/in/ramon-pereira88/) 🤝
