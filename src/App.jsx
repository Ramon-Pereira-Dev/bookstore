import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Form from './pages/Form/Form';
import Books from './pages/Books/Books';
import Error from './components/Error/Error';

function App() {
  return (
    <Router> 
      <Routes>
        <Route path="/" element={<Form />} />
        <Route path="/books" element={<Books />} />      
        <Route path="*" element={<Error />} />
      </Routes>          
    </Router>
  );
}

export default App;
