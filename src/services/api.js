import axios from 'axios'

// export const api = axios.create({
//    baseURL: 'https://books.ioasys.com.br/api/v1/'   
// }) 

// api.defaults.headers.common['Authorization'] = 'Bearer ' + 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MWM5YzI5MGNjNDk4YjVjMDg4NDVlMGEiLCJ2bGQiOjE2NDcyNTYwNzMwMDAsImlhdCI6MTY0NzI1OTY3MzAwMH0.OJoz9lJQewcQu_pXuYpdb-2U92sbtsUUzS8RSdUGHW4'

export const api = axios.create({
   baseURL: 'https://books.ioasys.com.br/api/v1/'   
}) 

api.interceptors.request.use((config) => {
  const token = window.localStorage.getItem('token')
  if (!token) return config;
  if (config?.headers) {
    config.headers = { Authorization: `Bearer ${token}` };
  }
  return config;
});

export default api