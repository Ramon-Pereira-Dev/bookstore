import styles from './Header.module.css'
import { Link } from 'react-router-dom'
import { MdOutlineLogout } from 'react-icons/md'

const Header = () => {
  return (
    <>
      <header className={styles.title}>
         <div className={styles.logo}>
               <p><strong>ioasys</strong></p>
               <p>Books</p>
         </div>

         <div className={styles.user}>
            <p>Bem vindo,</p>
            <p><strong>Ramon Pereira!</strong></p>
         </div>
         <Link to="/" className={styles.logoutBtn}>
           <MdOutlineLogout className={styles.icon} />
         </Link>
      </header>
    </>
  )
}

export default Header