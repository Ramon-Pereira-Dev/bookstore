import React from 'react'
import styles from './Pagination.module.css'
import { BsChevronLeft } from "react-icons/bs";
import { BsChevronRight } from "react-icons/bs";

const Pagination = ({ page, changePage, totalPages }) => {

   function handleChangePage(type, page) {
      if (type === 'previous') {
         if(page > 1) {
            changePage(page - 1)       
         }
      } 
      if (type === 'next') {
         if (page < totalPages) {
            changePage(page + 1)
         }
      }
   }
   
  return (
    <div className={styles.paginate}>
       Página {page} de {totalPages} 
       <button className={ page === 1 ? styles.invalid : styles.prev } onClick={() => handleChangePage('previous', page) }> <BsChevronLeft /> </button>
       <button className={ page === totalPages ? styles.invalid : styles.next} onClick={() => handleChangePage('next', page) } > <BsChevronRight /> </button>
    </div>
  )
}

export default Pagination