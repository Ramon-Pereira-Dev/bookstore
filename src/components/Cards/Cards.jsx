import { useState } from 'react'
import styles from './Cards.module.css'
import Modal from 'react-modal'

const Cards = ({ 
   title, authors, id, imageUrl, pageCount, publisher , published
 }) => {
   const [bookPreviewModal, setBookPreviewModal] = useState(false)

   function handleBookPreviewModal() {
      setBookPreviewModal(true);
   }

   function handleCloseBookPreviewModal() {
      setBookPreviewModal(false);
   }

  return (
     <>
      <div className={styles.bookCard} onClick={handleBookPreviewModal} >
         <div className={styles.book} style={{
            backgroundImage: `url(${imageUrl})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
         }}></div>
         <div className={styles.bookInfo}>
            <div className={styles.bookTitle}>{title}</div>
            <div className={styles.bookAuthor}>{authors.join(', ')}</div> 
            <div className={styles.bookPages}>{pageCount} páginas</div> 
            <div className={styles.bookEditor}>{publisher}</div> 
            <div className={styles.bookPublish}>Publicado em {published}</div> 
         </div>
      </div>

      <Modal 
         isOpen={bookPreviewModal}
         onRequestClose={handleCloseBookPreviewModal}
         overlayClassName={styles.reactModalOverlay}
         className={styles.overlayContent}
      >
         <div className={styles.bookModalCard} onClick={handleBookPreviewModal} >
            <div className={styles.bookModal} style={{
               backgroundImage: `url(${imageUrl})`,
               backgroundRepeat: 'no-repeat',
               backgroundSize: 'cover',
            }}></div>
            <div className={styles.bookInfoModal}>
               <div className={styles.bookTitleModal}>{title}</div>
               <div className={styles.bookAuthorModal}>{authors.join(', ')}</div> 
               <h2 className={styles.infoModal}>Informações</h2>
               <div className={styles.bookPagesModal}>{pageCount} páginas</div> 
               <div className={styles.bookEditorModal}>{publisher}</div> 
               <div className={styles.bookPublishModal}>Publicado em {published}</div>
            </div>
         </div>
      </Modal>
     </>
  )
}

export default Cards