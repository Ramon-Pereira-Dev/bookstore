import { useState } from 'react'
import styles from './Form.module.css'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import api from '../../services/api'
import { useNavigate } from 'react-router-dom'

const Form = () => {
   const navigate = useNavigate()   
   const [validate, setValidate] = useState('')

   async function handleSubmit(values) {
      try {
         const { data, headers } = await api({
            url: 'https://books.ioasys.com.br/api/v1/auth/sign-in',
            method: 'POST',
            data: { "email": values.email, "password": values.password }
         })
         console.log('data', data)
         console.log('headers', headers)
         window.localStorage.setItem('token', headers.authorization)
         window.localStorage.setItem('refresh-token', headers['refresh-token'])
         window.localStorage.setItem('user', data.name)
         navigate('/books')
      } catch (error) {
         setValidate('Email e/ou senha incorretos!')
         console.log('error', error)
      }
   }

   const formik = useFormik({
      initialValues: {
         email: '',
         password: ''
      },
      validationSchema: Yup.object({
         email: 
            Yup
               .string()
               .required('O campo email é obrigatório!'),
         password: 
            Yup
               .string()
               .required('O campo senha é obrigatório!')
      }), 
      onSubmit: values => {
         handleSubmit(values)         
      }
   });
      
  return (
    <>
      <div className={styles.container} style={{ 
        backgroundImage: 'url(/img/Background_Image.png)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }}>
         <form className={styles.form} onSubmit={formik.handleSubmit}>
            <div className={styles.title}>
               <p><strong>ioasys</strong></p>
               <p>Books</p>
            </div>
            <div className={styles.emailWrapper}>
               <label className={styles.label}>Email</label>
               <input 
                  type="text" 
                  name="email"
                  autoComplete='off' 
                  className={styles.inputEmail}
                  value={formik.values.email}
                  onChange={formik.handleChange}
               />
            </div>
            <div className={styles.passwordWrapper}>
               <label htmlFor="password" className={styles.label}>Senha</label>
               <div className={styles.password}>
                  <input 
                     type="password" 
                     name="password" 
                     className={styles.inputPass} 
                     value={formik.values.password}
                     onChange={formik.handleChange}
                  />
                  { validate &&
                     <small className={styles.errors}>{validate}</small>
                  }
                  <button type="submit" onClick={handleSubmit} className={styles.btn}>Entrar</button>
               </div>
            </div>
         </form>
      </div>   
    </>
  )
}

export default Form