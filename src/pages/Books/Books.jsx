import { useState, useEffect } from 'react'
import Header from '../../components/Header/Header'
import styles from './Books.module.css'
import Cards from '../../components/Cards/Cards'
import { api } from '../../services/api'
import Pagination from '../../components/Pagination/Pagination'

const Books = () => {
  const [cards, setCards] = useState([])
  const [page, setPage] = useState(1)
  const [totalPages, setTotalPages] = useState(0)

  useEffect(() => {
    loadCards()
 }, [])

   useEffect(() => {
      loadCards()
   }, [page])

   async function loadCards() {
    const response = await api.get(`/books?page=${page}&amount=12`)
    console.log(response.data)
    setCards(response.data.data)
    setTotalPages(Math.ceil(response.data.totalPages))
  }

  return (
    <>
      <div className={styles.container}>
        <div className={styles.bg} style={{ 
          backgroundImage: 'url(/img/books-bg.png)',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
        }}>
        <Header />
        <div className={styles.cardsContainer}>
          {cards.map(card => (
            <Cards 
              key={card.id}
              id={card.id}
              title={card.title}
              authors={card.authors}
              publisher={card.publisher}
              pageCount={card.pageCount}
              published={card.published}
              imageUrl={card.imageUrl}
            />
          ))}
        </div>
        <Pagination page={page} totalPages={totalPages} changePage={setPage}/>
        </div>
      </div>
    </>
  )
}

export default Books